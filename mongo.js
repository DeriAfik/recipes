const MongoClient = require('mongodb').MongoClient;
const mongoConf = require('./config/app').mongo;

module.exports = (callback) => {
    const connectionString = `mongodb://${mongoConf.user}:${mongoConf.password}@ds255309.mlab.com:55309/recipes`;
    MongoClient.connect(connectionString, (err, client) => {
        if (err) return console.log(err)
        db = client.db('recipes');
        callback(db);
    });
}