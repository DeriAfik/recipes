// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

var pusher = new Pusher('f8791628502497f3ad66', {
    cluster: 'ap2',
    encrypted: true
});

var channel = pusher.subscribe('recipes-channel');
channel.bind('new-recipe', function(data) {
    notifyMe(data.recipe.name);
});

if (!Notification) {
    alert('Desktop notifications not available in your browser. Try Chromium.'); 
}

if (Notification.permission !== "granted")
    Notification.requestPermission();

function notifyMe(recipeName) {
    if (Notification.permission !== "granted") {
        Notification.requestPermission();
    } else {
        var notification = new Notification('New recipe: ' + recipeName, {
            icon: 'https://www.shareicon.net/download/2016/06/16/595363_ingredient.ico',
            body: 'Please refresh your page to see the chnages',
        });

        notification.onclick = function () {
            window.open("http://localhost:3000/recipes");      
        };
    }
}