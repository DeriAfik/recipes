const ApplicationController = require('../controllers/application_controller');
const RecipesController = require('../controllers/recipes_controller');
const ShoppingListController = require('../controllers/shopping_list_controller');
const ShopLocationsController = require('../controllers/shop_locations_controller');

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index');
// });

module.exports = function routes(app, express) {
  // Recipes routes
  app.get('/api/recipes', RecipesController.all);
  app.get('/api/recipes/:id', RecipesController.getById);
  app.post('/api/recipes', RecipesController.store);
  app.delete('/api/recipes', RecipesController.delete);
  app.put('/api/recipes', RecipesController.update);

  // Locations routes
  app.get('/api/locations', ShopLocationsController.locations);
  app.get('/api/locations/search', ShopLocationsController.getByIngredientNameAndAmount);

  // Shopping list routes
  app.get('/api/shopping-list/ingredients', ShoppingListController.all);
  app.post('/api/shopping-list/ingredients', ShoppingListController.store);
  app.delete('/api/shopping-list/ingredients', ShoppingListController.delete);
  app.put('/api/shopping-list/ingredients', ShoppingListController.update);

  // Catch all route (return the base home page)
  app.get('*', ApplicationController.home);
};