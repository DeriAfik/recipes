var mongoSetup = require('../mongo');

/**
 * Get a list of all the available shop locations
 */
module.exports.locations = function locations(req, res) {
    mongoSetup((db) => {
        db.collection('locations').find().toArray((err, results) => {
            if (err) sendError(res);

            res.json(results);
        });
    });
};

/**
 * Get shops that have the required ingredient with the required amount
 */
module.exports.getByIngredientNameAndAmount = function getByIngredientNameAndAmount(req, res) {
    mongoSetup((db) => {
        const {
            name, amount, cityName
        } = req.query;

        db.collection('locations')
            .find({
                'ingredients.name': name,
                'ingredients.amount': { $gte: parseInt(amount) },
                'cityName': cityName
            })
            .toArray((err, results) => {
                if (err) sendError(res);

                res.json(results);
            });
    });
}