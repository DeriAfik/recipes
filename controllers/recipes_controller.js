var mongoSetup = require('../mongo');
var Pusher = require('pusher');
var pusherConf = require('../config/app').pusher;
var ObjectID = require('mongodb').ObjectID;

const sendError = (res) => {
    res.json({ status: 'error', message: 'something went wrong' });
};

/**
 * Get all recipes
 */
module.exports.all = function all(req, res) {
    const db = req.app.get('mongo');
    mongoSetup((db) => {
        db.collection('recipes').find().toArray((err, results) => {
            if (err) sendError(res);

            res.json(results);
        });
    });
};

/**
 * Get a recipe by id
 */
module.exports.getById = function getById(req, res) {
    const db = req.app.get('mongo');
    mongoSetup((db) => {
        db.collection('recipes').findOne({ id: parseInt(req.params.id) }, (err, results) => {
            if (err) sendError(res);

            res.json(results);
        });
    });
};

/**
 * Store a new recipe
 */
module.exports.store = function store(req, res) {
    const db = req.app.get('mongo');
    mongoSetup((db) => {

        db.collection('recipes').save(req.body, (err, result) => {
            if (err) {
                console.log(err);
                sendError(result);
            }

            const pusher = new Pusher(Object.assign({}, pusherConf, { encrypted: true }));

            pusher.trigger('recipes-channel', 'new-recipe', { 'recipe': req.body });

            res.json(result);
        });
    });
};

module.exports.delete = function store(req, res) {
    const db = req.app.get('mongo');
    mongoSetup((db) => {

        console.log(req.query);

        const {
            id
        } = req.query;

        db.collection('recipes').remove({ "_id": ObjectID(id) }, (err, result) => {
            if (err) {
                console.log(err);
                sendError(result);
            }

            res.json(result);
        });
    });
};

module.exports.update = function updateRecipe(req, res) {
    const db = req.app.get('mongo');
    mongoSetup((db) => {

        console.log(req.body);

        const {
            _id, name, description, imagePath, ingredients
        } = req.body;

        db.collection('recipes').update(
            { "_id": ObjectID(_id) },
            { "name": name, "description": description, "imagePath": imagePath, "ingredients": ingredients }, (err, result) => {
                if (err) {
                    console.log(err);
                    sendError(result);
                }

                res.json(result);
            }
        );
    });
};