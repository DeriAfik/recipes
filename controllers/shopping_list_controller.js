var mongoSetup = require('../mongo');
var ObjectID = require('mongodb').ObjectID;

const sendError = (res) => {
    res.json({ status: 'error', message: 'something went wrong' });
};

/**
 * Get shopping list ingredients
 */
module.exports.all = function all(req, res) {
    mongoSetup((db) => {
        db.collection('ingredients').find().toArray((err, results) => {
            if (err) sendError(res);

            res.json(results);
        });
    });
};

/**
 * Store a new ingredient in the shopping list
 */
module.exports.store = function store(req, res) {
    const db = req.app.get('mongo');

    console.log(req.body);

    mongoSetup((db) => {
        db.collection('ingredients').save(req.body, (err, result) => {
            if (err) sendError(res);

            res.json(result);
        });
    });
};


module.exports.delete = function deleteIngredient(req, res) {
    const db = req.app.get('mongo');
    mongoSetup((db) => {

        console.log(req.query);

        const {
            id
        } = req.query;

        db.collection('ingredients').remove({ "_id": ObjectID(id) }, (err, result) => {
            if (err) {
                console.log(err);
                sendError(result);
            }

            res.json(result);
        });
    });
};


module.exports.update = function updateIngredient(req, res) {
    const db = req.app.get('mongo');
    mongoSetup((db) => {

        console.log(req.body);

        const {
            _id, amount, name
        } = req.body;

        db.collection('ingredients').update(
            { "_id": ObjectID(_id) },
            { "name": name, "amount": amount }, (err, result) => {
                if (err) {
                    console.log(err);
                    sendError(result);
                }

                res.json(result);
            }
        );
    });
};