var mongoSetup = require('../mongo');

/**
 * Home Page
 */
module.exports.home = function home(req, res) {
    res.render('index');
};
