## Recipes - Made with Node JS, express, Angular & mongoDB

#### Authors:
- Afik Deri
- Rotem Gani

#### Get started
```
# get into the project
cd recipes

# install npm dependencies
npm install

# start the server for dev (restart on every file change)
npm run build
npm start-watch

# start the server in production
npm start

# visit http://localhost:3000
That's it :)
```

#### API docs
```
# Get all recipes
GET /api/recipes

# Response
[
    {
        id: 123,
        created_at: '2018-08-12 10:10:00',
        created_by: 'Martin Shumi',
        title: 'The best recipe in the world',
        description: 'This is another recipe for shish kabab',
        main_image: 'http://images.com/kabab.jpg',
        ingredients: [
            "1 tsp suger",
            "2 cups of water",
            "3 cans of redbull"
        ]
    },
    ...
]

# Get one recipe
GET /api/recipes/{id}

# Response
{
    id: 123,
    created_at: '2018-08-12 10:10:00',
    created_by: 'Martin Shumi',
    title: 'The best recipe in the world',
    description: 'This is another recipe for shish kabab',
    main_image: 'http://images.com/kabab.jpg',
    ingredients: [
        "1 tsp suger",
        "2 cups of water",
        "3 cans of redbull"
    ]
}

# create a new recipe
POST /api/recipes

# Request body 
{
    created_by: 'Martin Shumi',
    title: 'The best recipe in the world',
    description: 'This is another recipe for shish kabab',
    main_image: 'http://images.com/kabab.jpg',
    ingredients: [
        "1 tsp suger",
        "2 cups of water",
        "3 cans of redbull"
    ]
}

# Response success
{
    status: 'created',
    recipe: {
        id: 123,
        created_at: '2018-08-12 10:10:00',
        created_by: 'Martin Shumi',
        title: 'The best recipe in the world',
        description: 'This is another recipe for shish kabab',
        main_image: 'http://images.com/kabab.jpg',
        ingredients: [
            "1 tsp suger",
            "2 cups of water",
            "3 cans of redbull"
        ]
    }
}

# Response error
{
    status: 'error',
    message: 'something went wrong'
}

# Get all locations
GET /api/locations

# Response
[
    {
        name: "Tel Aviv",
        lat: "-25.363",
        lng: "131.044"
    },
    ...
]
```