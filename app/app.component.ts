import { Component, OnInit } from '@angular/core';

import { ShopLocationsService } from './services/shop-location.service';
import { DataStorageService } from './services/data-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadedFeature = 'recipe';

  constructor(private shoopingLocationsService: ShopLocationsService,
    private dataStorageService: DataStorageService) {
    this.dataStorageService.getRecipesFromDB();
    this.dataStorageService.getShoppingListFromDB();
    this.dataStorageService.getShopLocationsFromDB();
    this.dataStorageService.getNewsRemote();
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }

  ngOnInit() {

  }
}
