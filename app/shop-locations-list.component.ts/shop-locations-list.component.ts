import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GoogleMapsAPIWrapper } from '@agm/core';
import { ShopLocationsService } from '../services/shop-location.service';
import { ShoppingLocation } from '../models/shop-location.model';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-shop-locations-list',
  templateUrl: './shop-locations-list.component.html',
  styleUrls: ['./shop-locations-list.component.css'],
  providers: []
})
export class ShopLocationsListComponent implements OnInit, OnDestroy {
  @ViewChild('f') slForm: NgForm;
  private subscription: Subscription;

  locations: ShoppingLocation[];

  fullAmount: boolean = true;

  constructor(private shoopingLocationsService: ShopLocationsService) { }

  ngOnInit() {
    this.locations = this.shoopingLocationsService.getAllShoppingLocations();

    this.subscription = this.shoopingLocationsService.shoppingLocationsChanged
      .subscribe(
        (locations: ShoppingLocation[]) => {
          this.locations = locations;
        }
      )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onSubmit(form: NgForm) {
    const value = form.value;
    const requiredName = value.ingredientName;
    const requiredAmount = value.requiredAmount;
    const cityName = value.cityName;

    this.shoopingLocationsService.setShoppingLocationsByIngredient(requiredName, requiredAmount, cityName);
  }

  onClear() {
    this.slForm.reset();
    this.shoopingLocationsService.setShoppingLocationsAsFull();
  }
}
