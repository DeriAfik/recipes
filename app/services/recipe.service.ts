import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { Recipe } from '../models/recipe.model';
import { Ingredient } from '../models/ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Http, Response } from '@angular/http';

@Injectable()
export class RecipeService {
    recipesChanged = new Subject<Recipe[]>();

    private recipes: Recipe[] = [];

    constructor(private slService: ShoppingListService, private http: Http) { }

    setRecipes(recipes: Recipe[]) {
        this.recipes = recipes;
        this.recipesChanged.next(this.recipes.slice());
    }

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index: number) {
        return this.recipes[index];
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
    }

    addRecipe(recipe: Recipe) {
        // save to db
        let url = '/api/recipes';
        this.http.post(url, recipe)
            .subscribe(res => {
                console.log(res.json())
            });

        // update local
        this.recipes.push(recipe);
        this.recipesChanged.next(this.recipes.slice());
    }

    updateRecipe(index: number, recipeId: string, newRecipe: Recipe) {
        newRecipe._id = recipeId;
        this.recipes[index] = newRecipe;

        // Update in the db
        let url = '/api/recipes';
        this.http.put(url, newRecipe)
            .subscribe(res => {
                console.log(res.json())
            });

        this.recipesChanged.next(this.recipes.slice());
    }

    deleteRecipe(index: number) {
        let recipeToDelete: Recipe = this.recipes[index];

        let url = `/api/recipes/?id=${recipeToDelete._id}`;

        this.http.delete(url)
            .subscribe(res => {
                console.log(res.json())
            });

        this.recipes.splice(index, 1);
        this.recipesChanged.next(this.recipes.slice());
    }
}
