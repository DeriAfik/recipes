import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { News } from '../models/news.model';
import { Subject } from 'rxjs';

@Injectable()
export class NewsService {
    newsChanged = new Subject<News[]>();

    private news: News[];

    constructor(private http: Http) { }

    getNews() {
        return this.news.slice();
    }

    setNews(news: News[]) {
        this.news = news;
        this.newsChanged.next(this.news.slice());
    }
}
