import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/Rx';

import { Recipe } from '../models/recipe.model';
import { RecipeEditComponent } from '../recipes/recipe-edit/recipe-edit.component';
import { Ingredient } from '../models/ingredient.model';
import { RecipeService } from './recipe.service';
import { NewsService } from './news.service';
import { ShoppingListService } from './shopping-list.service';
import { ShopLocationsService } from './shop-location.service';
import { News } from '../models/news.model';

@Injectable()
export class DataStorageService {
    constructor(private http: Http,
        private recipeService: RecipeService,
        private newsService: NewsService,
        private shoppingListService: ShoppingListService,
        private shopLocationsService: ShopLocationsService) { }

    getRecipesFromDB() {
        this.http.get("http://localhost:3000/api/recipes")
            .map((response: Response) => {
                const recipes: Recipe[] = response.json();
                for (let recipe of recipes) {
                    if (!recipe['ingredients']) {
                        recipe.ingredients = [];
                    }
                }

                return recipes;
            })
            .subscribe(
                (recipes: Recipe[]) => {
                    this.recipeService.setRecipes(recipes);
                }
            );
    }

    getNewsRemote() {
        this.http.get("https://newsapi.org/v2/top-headlines?country=us&category=health&apiKey=173f4318276c49bf8bb4b15a2f61e5a5")
            .map((response: Response) => {
                const news: News[] = response.json().articles;
                return news;
            })
            .subscribe(
                (news: News[]) => {
                    this.newsService.setNews(news);
                }
            );
    }

    storeRecipesInDB() {
        const recipes = this.recipeService.getRecipes();
        
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post("http://localhost:3000/api/recipes", recipes, options);
    }

    getShoppingListFromDB() {
        this.http.get("http://localhost:3000/api/shopping-list/ingredients")
            .subscribe(
                (response: Response) => {
                    const ingredients = response.json();
                    this.shoppingListService.setIngredients(ingredients);
                }
            );
    }

    storeIngredientsShoppingListInDB() {
        const ingredients = this.shoppingListService.getIngredients();
        return this.http.post("http://localhost:3000/api/shopping-list/ingredients", ingredients);
    }

    getShopLocationsFromDB() {
        this.http.get("http://localhost:3000/api/locations")
            .subscribe(
                (response: Response) => {
                    const locations = response.json();
                    this.shopLocationsService.setLocations(locations);
                }
            );
    }

    getShopsByIngredientAndAmount(ingredientName: String, wantedAmount: number) {
        let URL = "http://localhost:3000/api/locations/ingredients/names/" + ingredientName + 
        "/amounts/"+ wantedAmount;

        this.http.get(URL)
            .subscribe(
                (response: Response) => {
                    const locations = response.json();
                    this.shopLocationsService.setLocations(locations);
                }
            );
    }
}
