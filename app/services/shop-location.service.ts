import { Injectable } from '@angular/core';
import { ShoppingLocation } from '../models/shop-location.model';
import { Ingredient } from '../models/ingredient.model';
import { Subject } from 'rxjs/Subject';
import { Http, Response } from '@angular/http';

@Injectable()
export class ShopLocationsService {
    shoppingLocationsChanged = new Subject<ShoppingLocation[]>();

    private allShoppingLocations: ShoppingLocation[] = [];

    private currentShoppingLocations: ShoppingLocation[];

    constructor(private http: Http) { 
        this.currentShoppingLocations = this.allShoppingLocations.slice();
    }

    setLocations(locations: ShoppingLocation[]) {
        this.allShoppingLocations = locations;
        this.currentShoppingLocations = this.allShoppingLocations.slice();
    }

    getCurrentShoppingLocations() {
        return this.currentShoppingLocations.slice();
    }

    getAllShoppingLocations() {
        return this.allShoppingLocations.slice();
    }

    getLocationsBy(name: string, amount: number, cityName: string) {
        let url = '/api/locations/search';

        if (name || amount || cityName) {
            url += `?name=${name}&amount=${amount}&cityName=${cityName}`;
        }

        this.http.get(url)
        .map((response: Response) => {
            const locations: ShoppingLocation[] = response.json();
            for (let location of locations) {
                if (!location['ingredients']) {
                    location.ingredients = [];
                }
            }

            return locations;
        })
        .subscribe(
            (locations: ShoppingLocation[]) => {
                this.currentShoppingLocations = locations;
                this.shoppingLocationsChanged.next(this.currentShoppingLocations.slice());
            }
        );
    }

    setShoppingLocationsByIngredient(name: string, amount: number, cityName: string) {
        this.getLocationsBy(name, amount, cityName);
    }

    setShoppingLocationsAsFull() {
        this.currentShoppingLocations = this.allShoppingLocations.slice();
        this.shoppingLocationsChanged.next(this.currentShoppingLocations.slice());
    }
}
