import { Ingredient } from "../models/ingredient.model";
import { Subject } from "rxjs/Subject";
import { Injectable } from "@angular/core";
import { Http, Response } from '@angular/http';

import 'rxjs/Rx';

@Injectable()
export class ShoppingListService {
    ingredientsChanged = new Subject<Ingredient[]>();
    startedEditing = new Subject<number>();

    private ingredients: Ingredient[] = [];

    constructor(private http: Http) {}

    setIngredients(ingredients: Ingredient[]) {
        this.ingredients = ingredients;
        this.ingredientsChanged.next(this.getIngredients());
    }

    getIngredients() {
        return this.ingredients.slice();
    }

    addIngredient(ingredient: any) {
        this._addIngredient(ingredient);
        this.ingredientsChanged.next(this.getIngredients());
    }

    _addIngredient(ingredient: Ingredient) {
        let objIndex = this.ingredients.findIndex((curr => curr.name == ingredient.name));
        if (objIndex != -1) {
            this.ingredients[objIndex].amount += ingredient.amount;

            // Update the ingredient in the db
            let url = '/api/shopping-list/ingredients';
            this.http.put(url, this.ingredients[objIndex])
                .subscribe(res => {
                    console.log(res.json())
                });
        } else {
           // Add the new ingredient to the db
            let url = '/api/shopping-list/ingredients';
            this.http.post(url, ingredient)
                .subscribe(res => {
                    console.log(res.json())
                });

            this.ingredients.push(ingredient);
        }
    }

    addIngredients(ingredients: Ingredient[]) {
        for (let curr of ingredients) {
            this.addIngredient(curr);
        }

        // this.ingredientsChanged.next(this.getIngredients());
    }

    getIngredient(index: number) {
        return this.ingredients[index];
    }

    updateIngredient(index: number, newIngredient: any) {
        this.ingredients[index] = newIngredient;
        this.ingredientsChanged.next(this.ingredients.slice());
    }

    deleteIngredient(index: number) {
        let ingredientToDelete: Ingredient = this.ingredients[index];

        let url = `/api/shopping-list/ingredients/?id=${ingredientToDelete._id}`;

        this.http.delete(url)
            .subscribe(res => {
                console.log(res.json())
            });

        this.ingredients.splice(index, 1);
        this.ingredientsChanged.next(this.ingredients.slice());
    }
}