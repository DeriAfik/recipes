import { Component, OnInit } from '@angular/core';

import { ShopLocationsService } from '../../services/shop-location.service';

@Component({
  selector: 'app-google-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  lat: number = 32.0666;
  lng: number = 34.7650;
  zoom: number = 8;

  mapMarkers = [];

  constructor(private shoopingLocationsService: ShopLocationsService) { }

  ngOnInit() {
    this.mapMarkers = [];

    for (let currentLocation of this.shoopingLocationsService.getAllShoppingLocations()) {
      this.mapMarkers.push({
        lat: currentLocation.lat,
        lng: currentLocation.lng,
        label: currentLocation.name
      });
    }
  }
}
