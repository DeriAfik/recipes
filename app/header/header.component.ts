import { Component, ElementRef, ViewChild } from '@angular/core';
import { Response } from '@angular/http';

import { DataStorageService } from '../services/data-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  rectW:number = 100;
  rectH:number = 100;
  rectColor:string = "#FF0000";
  context:CanvasRenderingContext2D;

  @ViewChild("logo") logo;

  constructor(private dataStorageService: DataStorageService) {}

  ngAfterViewInit() {
    let canvas = this.logo.nativeElement;
    this.context = canvas.getContext("2d");
    this.context.font = "30px Arial";
    this.context.fillText("Recipes", 20, 35);
  }

  onSaveData() {
    this.dataStorageService.storeRecipesInDB()
      .subscribe(
        (response: Response) => {
          console.log(response);
        }
      );

      this.dataStorageService.storeIngredientsShoppingListInDB()
      .subscribe(
        (response: Response) => {
          console.log(response);
        }
      );
  }

  onFetchData() {
    this.dataStorageService.getRecipesFromDB();
    this.dataStorageService.getShoppingListFromDB();
    this.dataStorageService.getShopLocationsFromDB();
  }
}
