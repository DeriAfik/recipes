import { Component, OnInit, OnDestroy } from '@angular/core';

import { Ingredient } from '../models/ingredient.model';
import { ShoppingListService } from '../services/shopping-list.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  ingredients: Ingredient[];
  private subscription: Subscription;

  private chartData: Array<any>;

  constructor(private shoopingListService: ShoppingListService) { }

  ngOnInit() {
    this.ingredients = this.shoopingListService.getIngredients();
    this.subscription = this.shoopingListService.ingredientsChanged
      .subscribe(
        (ingredients: Ingredient[]) => {
          this.ingredients = ingredients;
          this.generateChartData();
        }
      )

    // give everything a chance to get loaded before starting the animation to reduce choppiness	   
    setTimeout(() => {
      this.generateChartData();
      // change the data periodically	      
      setInterval(() => this.generateChartData(), 3000);
    }, 1000);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onEditItem(index: number) {
    this.shoopingListService.startedEditing.next(index);
  }

  generateChartData() {
    this.chartData = [];

    for (let currentIngredient of this.ingredients) {
      this.chartData.push([currentIngredient.name, currentIngredient.amount]);
    }
  }
}
