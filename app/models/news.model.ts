class Source {
    public id: string;
    public name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }
}

export class News {
    public source: Source;
    public author: string;
    public description: string;
    public url: string;
    public urlToImage: string;
    public publishedAt: string;

    constructor(source: Source, author: string, description: string, url: string, urlToImage: string, publishedAt: string) {
        this.source = source;
        this.author = author;
        this.description = description;
        this.url = url;
        this.urlToImage = urlToImage;
        this.publishedAt = publishedAt;
    }
}
