import { Ingredient } from "../models/ingredient.model";

export class ShoppingLocation {
  public name: string;
  public description: string;
  public lat: number;
  public lng: number;
  public ingredients: Ingredient[];

  constructor(name: string, desc: string, lat: number, lng: number, ingredients: Ingredient[]) {
    this.name = name;
    this.description = desc;
    this.lat = lat;
    this.lng = lng;
    this.ingredients = ingredients;
  }
}
