import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { NewsService } from '../services/news.service';
import { News } from '../models/news.model';

@Component({
  selector: 'news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  news: News[];

  constructor(private newsSerive: NewsService) {}

  ngOnInit() {
    this.subscription = this.newsSerive.newsChanged
      .subscribe(
        (news: News[]) => {
          this.news = news;
        }
      );

    this.news = this.newsSerive.getNews();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
